# make-dynpart-mappings

This is a short program that sets up Android dynamic partitions. It was created
for postmarketOS so that it can use dynamic partitions without pulling too many
dependencies.

This is not a complete substitute for fs\_mgr (*yet*). It doesn't have any
options either and accepts one argument.

## Compiling

You can build this program using make:

    $ make

## License

This program as a whole is licensed under the GNU GPLv3. It also includes some
3rd party code in the folder `3rdparty/`, licensed under the Apache license,
version 2.0.

## 3rd Party Code

The 3rd party code used to define the format of the dynamic partitions can be
found [here](https://android.googlesource.com/platform/system/core/+/8edafa239369ea6956d719dbf60b4ce36f9e42e9/fs_mgr/liblp/include/liblp/metadata_format.h).
